package com.designpattern.shirish.creational.Builder;

public class URL {

	public URL(URLBuilder urlBuilder) {
		this.port = urlBuilder.port;
		this.hostname = urlBuilder.hostname;
		this.parameter = urlBuilder.parameter;
		
	}
	
	public String getUrl() {
		return this.hostname+":"+this.port+"/"+this.parameter;
	}

	public String hostname;
	public String port;
	public String parameter;
	
	
	
	public static class URLBuilder {
		public String hostname;
		public String port;
		public String parameter;
		
		public URLBuilder(String hostname, String port, String parameter) {
			super();
			this.hostname = hostname;
			this.port = port;
			this.parameter = parameter;
		}
		
		public URL build()
		{
			return new URL(this);
					
		}
		
		
	}
	
	
}
