package com.designpattern.shirish.creational.Builder;

public class TestBuilderMain {

	public static void main(String[] args) {
	
		URL url = new URL.URLBuilder("localhost", "8000", "shirIsh").build();
		System.out.println("URL IS " + url.getUrl());

	}

}
