package com.designpattern.shirish.creational.Factory;

public interface Check {

	public String getCheckName();
	
}
