package com.designpattern.shirish.creational.Factory;

public class CheckFactory {
	
	public static Check checkInstance;
	
	public static Check getCheckInstance(String checkName)
	{
		switch(checkName.toLowerCase())
		{
		case "duplicate": {
			checkInstance =  new DuplicateCheck();
			break;
		}
		case "red" :
		{
			checkInstance = new RedClientCheclk();
			break;
		}
	    default:
			checkInstance = null;
			break;
		}
		return checkInstance;
	}
}
