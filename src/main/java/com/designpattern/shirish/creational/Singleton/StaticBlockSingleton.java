package com.designpattern.shirish.creational.Singleton;

public class StaticBlockSingleton {
	
	private static StaticBlockSingleton instance;
	 
	private StaticBlockSingleton() {
		
	}
	
	static {
		try {
			if(instance == null)
			{
				instance = new StaticBlockSingleton();
			}
			
		} catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public StaticBlockSingleton getInstance() {
		return instance;
	}

}
