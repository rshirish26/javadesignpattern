package com.designpattern.shirish.creational.Singleton;

public class EarlyInitializationSingleton {
	
	private static final EarlyInitializationSingleton instance = new EarlyInitializationSingleton();
	
    private EarlyInitializationSingleton() {
    	
    };	
    
    public static EarlyInitializationSingleton getInstance()
    {
		return instance;
    	
    }

}
