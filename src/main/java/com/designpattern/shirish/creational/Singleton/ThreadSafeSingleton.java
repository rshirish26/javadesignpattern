package com.designpattern.shirish.creational.Singleton;

public class ThreadSafeSingleton {
	
	private static ThreadSafeSingleton instance;
	
	private ThreadSafeSingleton() {
		
	}
	
//	public static synchronized ThreadSafeSingleton getInstance()
//	{
//		if(null== instance)
//		{
//			instance = new ThreadSafeSingleton();
//		} 
//		return instance;
//	}
	// double checking
	public static ThreadSafeSingleton getInstance()
	{
		if(null== instance)
		{
			synchronized (ThreadSafeSingleton.class) {
				if(null== instance)
				instance = new ThreadSafeSingleton();
					}
			
		} 
		return instance;
	}

}
