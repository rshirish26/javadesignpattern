package com.designpattern.shirish.structural.decorator;

public class DecoratorPatternTest {

	public static void main(String[] args) {
		NostroDetail nsotm = new NostroDetailFromOTM(new NostroDetailFromDatabase(new NostroDetailBasic()));
		nsotm.enrich();

	}

}
