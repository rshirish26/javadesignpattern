package com.designpattern.shirish.structural.decorator;

public class NostroDetailEnricher implements NostroDetail {
	
	protected NostroDetail nostroDetailObject;
	
	public NostroDetailEnricher(NostroDetail nostroDetailObject) {
		//super();
		this.nostroDetailObject = nostroDetailObject;
	}

	public void enrich() {
		this.nostroDetailObject.enrich();
	}

}
