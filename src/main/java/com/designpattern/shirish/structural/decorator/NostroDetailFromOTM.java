package com.designpattern.shirish.structural.decorator;

public class NostroDetailFromOTM extends NostroDetailEnricher {
	

	public NostroDetailFromOTM(NostroDetail nostroDetailObject) {
		super(nostroDetailObject);	
	}
	
	public void enrich() {
		super.enrich();
		System.out.println("Details from OTM");
	}
	
}
