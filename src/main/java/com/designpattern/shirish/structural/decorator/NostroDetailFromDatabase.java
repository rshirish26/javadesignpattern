package com.designpattern.shirish.structural.decorator;

public class NostroDetailFromDatabase extends NostroDetailEnricher {

	public NostroDetailFromDatabase(NostroDetail nostroDetailObject) {
		super(nostroDetailObject);
		// TODO Auto-generated constructor stub
	}
	
	public void enrich()
	{   
		super.enrich();
		System.out.println("Details from database");
	}

}
