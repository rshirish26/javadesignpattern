package com.designpattern.shirish.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component {

	protected List<Component> components = new ArrayList<Component>();

	protected String orderName;

	public Composite(String orderName) {
		super();
		this.orderName = orderName;
	}

	protected void addOrder(Component c) {
		components.add(c);
	}

	@Override
	public void getPriceOfOrder() {
		System.out.println("OrderName --" + orderName);
		for (Component c : components) {
			c.getPriceOfOrder();
		}

	}

}
