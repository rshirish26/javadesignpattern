package com.designpattern.shirish.structural.composite;

public class TestCompositePattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	    
		Component pocketOrder = new Leaf("pocketOrder", "abcd", 1234);
		Component linkedOrder = new Leaf("linkedOrder", "efgh", 1231);
		Component advisoryOrder = new Leaf("advisoryOrder", "ijk", 1232);
		
		
		Component forwardOrder = new Leaf("forwardOrder", "abcd", 1234);
		Component futureOrder = new Leaf("futureOrder", "efgh", 1231);
		Component optionOrder = new Leaf("optionOrder", "ijk", 1232);
		
		Composite equities = new Composite("Cash Equities");
		Composite derivatives = new Composite("Derivatives");
		
		equities.addOrder(pocketOrder);
		equities.addOrder(linkedOrder);
		equities.addOrder(advisoryOrder);
		
        derivatives.addOrder(forwardOrder);
        derivatives.addOrder(futureOrder);
        derivatives.addOrder(optionOrder);
        
        Composite allOrders = new Composite("Orders");
        allOrders.addOrder(equities);
        allOrders.addOrder(derivatives);
        
        allOrders.getPriceOfOrder();
	

	}

}
