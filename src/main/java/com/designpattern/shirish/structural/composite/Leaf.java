package com.designpattern.shirish.structural.composite;

public class Leaf implements Component {
	
	protected String orderName;
	protected String orderId;
	protected int orderPrice;
	
	public Leaf(String orderName, String orderId, int orderPrice) {
		super();
		this.orderName = orderName;
		this.orderId = orderId;
		this.orderPrice = orderPrice;
	}
	

	@Override
	public void getPriceOfOrder() {
		System.out.println(this.orderName+" -- "+this.orderId+" -- "+this.orderPrice);
		
	}

}
