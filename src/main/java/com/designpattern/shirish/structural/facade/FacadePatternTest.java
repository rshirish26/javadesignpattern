package com.designpattern.shirish.structural.facade;

import com.designpattern.shirish.structural.facade.HelperFacade.ReportType;

public class FacadePatternTest {
	
	public static void main(String args[])
	{
		HelperFacade.generateReportBasedOnInput(ReportType.HTML);
	}

}
