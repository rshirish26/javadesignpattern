package com.designpattern.shirish.structural.facade;

public class HTMLGeneratorHelper {
	
	public void generateReport()
	{
		System.out.println("Generate HTML Report");
	}

}
