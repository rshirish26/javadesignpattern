package com.designpattern.shirish.structural.facade;



public class HelperFacade {
	
	public static void generateReportBasedOnInput(ReportType reportType)
	{
		switch(reportType)
		{
		case HTML:
		{
			HTMLGeneratorHelper htmlGeneratorHelper = new HTMLGeneratorHelper();
			htmlGeneratorHelper.generateReport();
			break;
		}
		case PDF:{
			PDFGeneratorHelper pdfGeneratorHelper = new PDFGeneratorHelper();
			pdfGeneratorHelper.generateReport();
			break;
		}
	}
	

}
	
public static enum ReportType
	{
		HTML, PDF;
	}
}
