package com.designpattern.shirish.bridge;

public class DerivativeTrade extends Trade {

	public DerivativeTrade(Action action) {
		super(action);
	}

	@Override
	public void doAction() {
      System.out.println("Derivative Trade");
      action.doAction();
      
	}

}
