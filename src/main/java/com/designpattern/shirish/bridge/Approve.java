package com.designpattern.shirish.bridge;

public class Approve implements Action {

	public void doAction() {
		System.out.println("Approve the trade");
	}

}
