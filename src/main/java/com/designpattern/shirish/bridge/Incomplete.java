package com.designpattern.shirish.bridge;

public class Incomplete implements Action {

	public void doAction() {
		System.out.println("Mark the trade as incomplete");
		
	}

}
