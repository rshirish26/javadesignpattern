package com.designpattern.shirish.bridge;

public interface Action {
	
	public void doAction();

}
