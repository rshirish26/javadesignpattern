package com.designpattern.shirish.bridge;

public class Block implements Action {

	public void doAction() {
		System.out.println("Block the trade");
	}

}
