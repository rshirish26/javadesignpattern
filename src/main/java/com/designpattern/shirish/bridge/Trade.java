package com.designpattern.shirish.bridge;

public abstract class Trade {
	
	protected Action action;
	
	public Trade(Action action) {
		super();
		this.action = action;
	}
   
	abstract public void doAction();
}
