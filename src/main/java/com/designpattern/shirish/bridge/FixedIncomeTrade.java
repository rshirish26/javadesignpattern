package com.designpattern.shirish.bridge;

public class FixedIncomeTrade extends Trade {

	public FixedIncomeTrade(Action action) {
		super(action);
		
	}

	@Override
	public void doAction() {
		System.out.println("Fixed Income Trade");
		action.doAction();	
	}
	
	

}
