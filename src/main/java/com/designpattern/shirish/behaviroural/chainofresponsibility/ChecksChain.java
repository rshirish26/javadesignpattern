package com.designpattern.shirish.behaviroural.chainofresponsibility;

public interface ChecksChain {
	
	public void setNextChain(ChecksChain check);
	public void doCheck(String input);

}
