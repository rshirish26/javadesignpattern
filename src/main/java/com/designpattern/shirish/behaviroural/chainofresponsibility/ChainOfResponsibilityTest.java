package com.designpattern.shirish.behaviroural.chainofresponsibility;

public class ChainOfResponsibilityTest {
	
	
	public static void main(String args[])
	{
		ChainConfig chainConfig = new ChainConfig();
		ChecksChain chain = chainConfig.getChainConfig();
		chain.doCheck("Shirish");
	}

}
