package com.designpattern.shirish.behaviroural.chainofresponsibility;

public class RedClientCheck implements ChecksChain {
	
	private ChecksChain check;

	@Override
	public void setNextChain(ChecksChain check) {
		this.check = check;
		
	}


	@Override
	public void doCheck(String input) {
		String newInput = input + "RedClient";
		System.out.println("Result:  " +newInput);
		
	}

}
