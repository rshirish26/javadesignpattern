package com.designpattern.shirish.behaviroural.chainofresponsibility;

public class ChainConfig {
	
	public ChecksChain getChainConfig() {
	
	ChecksChain c1 = new DupeCheck();
	ChecksChain c2 = new SuitabilityCheck();
	ChecksChain c3 = new RedClientCheck();
	c1.setNextChain(c2);
	c2.setNextChain(c3);
	return c1;
	
	}

}
