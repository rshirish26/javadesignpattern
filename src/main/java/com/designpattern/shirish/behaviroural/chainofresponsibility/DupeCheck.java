package com.designpattern.shirish.behaviroural.chainofresponsibility;

public class DupeCheck implements ChecksChain {

	private ChecksChain check;

	@Override
	public void setNextChain(ChecksChain check) {
		this.check = check;

	}

	@Override
	public void doCheck(String input) {
		String newInput = input + "dupecheck";
		this.check.doCheck(newInput);

	}

}
