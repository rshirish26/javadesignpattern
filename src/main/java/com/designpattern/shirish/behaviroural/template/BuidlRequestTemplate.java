package com.designpattern.shirish.behaviroural.template;

public abstract class BuidlRequestTemplate {
	
	public final void buildTemplateRequest() {
		enrichHostName();
		enrichEnrichPortNumber();
		enrichInputParameters();
		enrichRequestParameter();
	}

	public abstract void enrichRequestParameter(); 

	public abstract void enrichInputParameters();

	public abstract void enrichEnrichPortNumber(); 

	public abstract void enrichHostName() ;

}
